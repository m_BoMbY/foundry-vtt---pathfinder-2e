export const MAGIC_SCHOOLS = new Set([
    "abjuration",
    "conjuration",
    "divination",
    "enchantment",
    "evocation",
    "illusion",
    "necromancy",
    "transmutation",
] as const);

export const MAGIC_TRADITIONS = new Set(["arcane", "divine", "occult", "primal"] as const);
